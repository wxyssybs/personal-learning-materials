# axios
前端最流行的ajax请求库

# 简单使用
### json-server 的搭建
```js
/*
    1. 安装服务  npm install -g json-server
    2. 自定义数据
    3. 启动服务 json-server --watch db.json
    4. 服务延时返回数据 -d 2000
*/
```
## 使用axios
### 导入axios
```js
/*
标签引入 
    <script src="https://cdn.bootcdn.net/ajax/libs/axios/0.21.1/axios.min.js"></script>
```

### axios的使用
```js
// 一般使用
// 查
btns[0].onclick = function () {
    axios({
        method: 'GET',
        url: 'http://127.0.0.1:3000/posts/1'
    }).then(response => {
        console.log(response)
    })
}
// 增
btns[1].onclick = function () {
    axios({
        method: 'POST',
        url: 'http://127.0.0.1:3000/posts',
        data:{
            "title":'good job',
            "author":"wxyssybs",
        }
    }).then(response => {
        console.log(response)
    })
}
// 改
btns[2].onclick = function () {
    axios({
        method: 'PUT',
        url: 'http://127.0.0.1:3000/posts/1',
        data:{
            "title":'good job',
            "author":"wxyssybs",
        }
    }).then(response => {
        console.log(response)
    })
}
// 删
btns[3].onclick = function () {
    axios({
        method: 'DELETE',
        url: 'http://127.0.0.1:3000/posts/1',

    }).then(response => {
        console.log(response)
    })
}
// 其他使用方法 
// axios.request 和axios（）基本一样
btns[0].onclick = function () {
    axios.request({
        method: 'POST',
        url: 'http://127.0.0.1:3000/posts'
    }).then(response => {
        console.log(response.data)
    })
}
// axios.get
axios.get('http://127.0.0.1:3000/posts'
    ).then(response => {
        console.log(response.data)
    })
}
// axios.post(注意和上面比少个括号)
axios.post('http://127.0.0.1:3000/posts',
        {
            "title":'good job',
            "author":"wxyssybs",
        }
    ).then(response => {
        console.log(response.data)
    })
}
```
### axios默认配置
```js
/*
// 请求
axios.defaults.method = 'GET'
// 基本路由
axios.defaults.baseURL = 'http://127.0.0.1:3000'
// get参数
axios.defaults.params = { id: 2 }
// 超时时间
axios.defaults.timeout = 3000
*/
axios.defaults.method = 'GET'
axios.defaults.baseURL = 'http://127.0.0.1:3000'
axios.defaults.params = { id: 2 }
axios.defaults.timeout = 3000

btns[0].onclick = function () {

    axios({
        url: '/posts/',

    }).then(response => {
        console.log(response.data)
    })
}
```

### axios创建实例对象
```js

const duixiang = axios.create({
    baseURL :'http://127.0.0.1:3000/',
    timeout:2000,
    // method :'GET',
})

// 方法一
duixiang({
    url:'/posts'
}).then(datas=>{
    console.log(datas.data);
})

// 方法二
duixiang.get('/posts').then(data=>{
    console.log((data.data));
})
```
### axios 拦截器
```js
// 设置请求拦截器
axios.interceptors.request.use(function (config) {
    console.log('请求拦截器成功')
    return config;
}, function (error) {
    console.log('请求拦截器失败')

    return Promise.reject(error);
});
// 设置响应拦截器

axios.interceptors.response.use(function (response) {
    console.log('响应拦截器成功')

    return response.data;
}, function (error) {
    console.log('响应拦截器失败')

    return Promise.reject(error);
});
axios({
    method:'get',
    url:'http://127.0.0.1:3000/posts'
}).then(response=>{
    // console.log('自定义');
    console.log(response);
})
```

### axios 请求取消
```js
/*
1. 当请求的数据延时返回时，使用可以停止发送请求，
2. 也可以改进成多次发送同一个请求时，若上一次的请求未完成，则停止，发送新的请求
3. 下面的cancel 不是关键字，可以替换
*/
let cancel = null;
btns[4].onclick = function () {
    if (cancel !== null) {
        cancel();
    }
    axios({
        method: 'get',
        url: 'http://127.0.0.1:3000/posts',
        cancelToken: new axios.CancelToken(function (c) {
            cancel = c;
        })
    }).then(response => {
        console.log(response);
        cancel = null
    })
}
btns[5].onclick = function () {
    cancel();
}
```
# 源码分析(暂时不看)