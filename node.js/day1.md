# 什么是node.js

简单的说 Node.js 就是运行在服务端的 JavaScript。

Node.js 是一个基于Chrome JavaScript 运行时建立的一个平台。

# NPM - Node Package Manager

## npm 安装，卸载模块

```shell
npm install name -g
#g 表示全局安装
npm uninstall name -g
卸载
```

## 创建配置文件（package.json）

```
npm init -y
# y 代表一切默认yes
```

