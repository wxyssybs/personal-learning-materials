  vue学习内容：
  vue基础
  vue-cli
  vue-router
  vuex
  element-ui
  vue3


# 初识vue

- 
  ## vue是什么


  一套用于构建用户界面的渐进式javascript框架

- 
  ## vue 的特点



1. 组件化模式，提高代码复用率，切让代码更好维护
2. 声明式编码，开发人员无需直接操作dom，提高效率
3. 使用虚拟dom+diff算法，提高dom节点复用

# 基础内容

## 1. 模板语法

- 插值语法（常用标签体内容）

    ```
    {{name}}  双大括号引入
    ```
- 指令语法（常用标签属性）
  1. v-bind
      ```
      v-bind： 即 ：   将后面引号中的内容作为js表达式
      v-bind：href="url" 或 :href="url"
      ```

      ```
      <div id='test'>
          <h1> {{name}}</h1>
          <h2> {{age}}</h2>
          <a :href="url">jump</a>
      </div>
      <script type="text/javascript">
        new Vue({
            el: "#test",
            data: {
                'name': 'gaochen',
                'age': 18,
                'url': "http://www.baidu.com"
            }
        })
      </script>
      ```
      
## 2. 数据绑定
- 单向绑定

    ```
    v-bind
    数据只能够从data流向页面
    ```
    
- 双向绑定）
  ```
  v-model
  数据可以从页面流向data，也可以从data流向页面
  一般应用在表单类元素上（input,select...)
  v-model:value 可以简写为 v-model="**"
  
  	双向数据绑定:<input type="text" v-model="name">
  	双向数据绑定:<input type="text" v-model:value="name">
  ```
## 3. el和data 的两种写法

```
#el
const v = new Vue({
 el:"xxx"
})

和

v.$mount("xxx")
```

```
#data
#对象式
new Vue({
  ***
  data:{
  	xxx
  }
})
#函数式（在用到组件时，必须用函数式）
data(){
	console.log(this)#此处的this为vue实例对象（普通函数）
  return {
  	xxx
	}
}
```

注意：有vue管理的函数，一定不要写箭头函数

## 4.MVVM模型

- M：模型（model）：对应data中的数据
- V：视图（view）：模板
- VM：视图模型（ViewModel）：vue实例对象

1.data中的所有属性，都会出现在vm上

2.vm上的所有属性和vue原型上的所有属性，在vue模板中都可以直接使用

##  5.数据代理

```
Object.defineProperty(person,'age',{
	value:18,
	enumerable : true, #控制属性是否可以枚举（参与遍历），默认值false (for循环遍历)
	writable：true，#控制属性是否可以被修改，默认值false (object.key = value)
	configurable:true #控制属性是否可以被删除，默认值false （delete object.key）
})

```

## 6.事件

### 基础

1. 使用v-on：xxx或者@xxx 绑定事件，xxx为事件名称
2. 事件的回调需要配置在methods对象中，最终会在vm上
3. methods中配置的函数，不要用箭头函数（this指向改变）
4. 传参可以在方法后面加上（）

```
button v-on:click = "showinfo1">click1</button>     
<button @click = "showinfo2(66)">click2</button>
new Vue({
   el:"#test",
   data:{
     name:"gc"
   },
   methods:{
     showinfo1(){
       alert("qqq")
     },
     showinfo2(number){
       alert(number)
     }
   }
})
```

### 事件修饰符

  1. prevent：阻止默认事件
   ```
   <a href="xxxxx" @click.prevent = "**"></a>
   阻止默认跳转
   ```
  2. stop：阻止事件冒泡
   ```
   多层嵌套时，阻止事件冒泡
   @click.stop
   ```
  3. once：事件只触发一次
   ```
   点击次以后不再想使用
   @click.once
   ```
  4. capture：捕获模式
   ```
   和冒泡刚好相反
   @click.capture
   ```
  5. self：只用event.target是当前操作的元素时才会触发事件
   ```
   类似于阻止事件冒泡
   @click.self
   ```
  6. passive：事件默认行为立即执行，无需等待事件毁掉执行完毕
  ```
  类似于异步操作，无需默认事件等待
  @click.passive
  ```

### 常用按键别名

```
回车 => enter 
删除 => delete
退出 => esc
空格 => space
换行 => tab(必须配合keydown使用)
#使用方法
<input type="text" placeholder="enter" @keyup.ctrl.y="showinfo">        
```

## 7.计算属性

​	要用的属性不存在，要通过已有的属性计算得来

```
data:{
  firstname:'a',
  lastname:'b'
},
computed:{
fullname:{
//get作用：当有人读取fullname时，get调用，返回值作为其value
//get调用时机： 1.初次读取fullname 2.依赖的数据发生变化（firstname，lastname）
	get(){
		return this.firstname+this.lastname
	},
	//set调用时机： 当fullname被修改时
	set(value){
    const arr = value(处理后)
    this.firstname = arr[0]
		this.lastname = arr[1]
	}
}
}
```

## 8.监视属性

```
data:{
  ishot：true
},
watch：{
  ishot:{
  //初始化时让handler调用下，默认false
  immediate:true,
  //ishot修改时调用
    handler(new,old){
    xxx
  }

######################或者####################
const vm = new Vue({
	data:{
  ishot：true
},
})
vm.$watch('ishot',{
  handler(){
  }
})
```

- ###### 深度监视

  ​	 配置deep:true 可以监测对象内部值的变化

  ```
  data:{
    ishot：true
    a:{
    	b:'xx',
    	c:'xx'
    }
  },
  watch：{
  //默认deep为false a类似于地址，里面的对象改变，并不能引起a的变化，所以一般检测不到，想要监测，必须deep：true
  deep:true  
    a:{
    	handler(){
      xxx
    }
    }
  ```

  ## 9.条件渲染

  -  v-if
     	v-if=“表达式”
     	v-else-if=“表达式”
     	v-else = “表达式
  

    ```
    适用于切换频率低的场景
    不展示的dom元素直接被移除
    v-if，v-else-if，v-else可连用，但不能被打断
    <template> 只可以和v-if使用，不可以与v-show使用，作用：在不破坏原结构的前提下，将模块封装
    ```
  
  - v-show
  		v-show=“表达式”
  
  
    ```
  适用于切换频率高的场景
  不展示的dom元素不会被移除，只会被隐藏
    ```
  ## 9.基本列表

  -  v-for
  
  
    ```
    用于展示列表数据
    v-for="(item,index) in xxx" :key="yyy"
    可遍历：数组，对象，字符串
    ```
  
  - v-show
  		v-show=“表达式”
  
  
    ```
  适用于切换频率高的场景
  不展示的dom元素不会被移除，只会被隐藏
    ```

