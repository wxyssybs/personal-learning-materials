# 什么是logrotate？

Linux logrotate命令用于管理记录文件。

使用logrotate指令，可让你轻松管理系统所产生的记录文件。它提供自动替换，压缩，删除和邮寄记录文件，每个记录文件都可被设置成每日，每周或每月处理，也能在文件太大时立即处理。您必须自行编辑，指定配置文件，预设的配置文件存放在/etc目录下，文件名称为logrotate.conf

# 如何使用

## 1. 安装下载

  ```
  下载之前先看下电脑有没有安装  centos 默认安装（下载之前先看下电脑有没有安装  centos 默认安装）
  yum -y install logrotate
  ```

## 2. 使用

​	注意：都要使用绝对路径

​	1.选择一个需要拆分的日志文件（下例都用wxyssybs.log)

​	2.创建一个配置文件（下例都用conf）

​	下面是我用到的一些配置

  ```
  #日志文件所在目录
  /opt/code/python/wxyssybs.log {
      rotate 365  
      copytruncate
      dateext
      compress
      dateformat -%s
      olddir /opt/code/python/py_log
  }
  ```

 3. 校验配置文件是否可用

    ```
    logrotate -d conf
    ```


 4. 配置定时任务

    ```
    * * * * * /usr/sbin/logrotate -f /opt/code/python/conf >/dev/null 2>&1
    每分钟都去使用指定的拆分配置文件来拆分
    ```



## 3.参数详解

  ```
  常用
  compress     压缩所有已经切割出来的日志文件
  rotate 365  保留365个切割日志数量，若新加的日志，替换调最老的那个日志文件
  delaycompress  压缩所有版本，除了当前和下一个最近的
  daily,weekly,monthly 按指定计划轮换日志文件
  olddir "dir" --> 指定日志文件的旧版本放在 “dir” 中，绝对路径
  size='logsize' --> 在日志大小大于 logsize（例如 100K，4M）时轮换
  dateext 切割出来的日志文件带有时间后缀   -年-月-日
  dateformat   dateext的格式  现在只支持 %Y %m %d %s 这四个参数
  copytruncate  暂时不清楚


  不常用 
  endscript --> 标记 prerotate 或 postrotate 脚本的结束
  errors "emailid" --> 给指定邮箱发送错误通知
  missingok --> 如果日志文件丢失，不要显示错误
  notifempty --> 如果日志文件为空，则不轮换日志文件
  postrotate --> 引入一个在日志被轮换后执行的脚本
  prerotate --> 引入一个在日志被轮换前执行的脚本
  rotate 'n' --> 在轮换方案中包含日志的 n 个版本
  sharedscripts --> 对于整个日志组只运行一次脚本

  ```

