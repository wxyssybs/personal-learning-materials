# 什么是脚手架？ 
    	脚手架（Scaffold），其实是一种工具，帮我们可以快速生成项目的工程化结构；每个项目作出完成的效果不同，但是它们的基本工程化结构是相似的。既然相似，就没有必要每次都从零开始搭建，完全可以使用一些工具，帮助我们生产基本的工程化模板；不同的项目，在这个模板的基础之上进行项目开发或者进行些配置的简单修改即可；这样也可以间接保证项目的基本结构一致性，方便后期的维护；
    
    总结：脚手架让项目从搭建到开发，再到部署，整个流程变得快速和便捷；对于现在比较流行的三大框架都有属于自己的脚手架：
        Vue的脚手架：vue-cli,
        Angular的脚手架：angular-cli,
        React的脚手架：create-react-app

# 如何使用
## 配置
```shell
# 安装脚手架
npm install -g create-react-app
# 创建项目
create-react-app 项目名称
# 启动
npm start
# 或者
yarn start
```
## 文件说明
```js
/*
node_modules 依赖包
public 静态资源存放
    favicon.ico 网页的logo
    index.html 主页面，项目只有这一个html文件（里面的详细内容，查看code/脚手架/react_staging/01_脚手架自带文件/public/index.html）
    manifest.json 加壳时的配置文件
    robots.txt 爬虫规则文件
src
    App.css App.js的css文件
    App.js 创建的名为App的组件
    App.test.js 做测试，一般不用
    index.css 大家通用的样式
    index.js 入口文件
        <React.StrictMode>// 检测代码中的不合理
    logo.svg 大logo
    reportWebVitals 检测性能
    setup Test.js 组件测试

*/
```
## get到的知识点

```js
/*
1. 在脚手架中js文件可能是组件，也可能是普通的js文件，为了区分，有如下方法：
    1. 组件的文件首字母大写
    2. 组件的js后缀改成jsx

2. 组件化编程：
    1. 所有的组件最好放在和App.js同级的components(自建)文件夹中
    2. 一个组件对应一个文件夹，
    3. 文件夹内有对应的js/jsx文件，或者css文件，
    4. 如果在导入的时候只引入到该组件的根目录级别，没有详细的组件文件，则默认导入index文件
3. 文件内部
    1. 文件导入
        1. {}的引入方式说明源文件中有多种暴露，而不是常用的赋值，例如（详见code/脚手架/react_staging/src/components/Hello/index.jsx）
        import React,{Component} from 'react'；
        该方式是因为 Component组件在react中有其他的暴露方式，并不是因为赋值
        2. 引入文件的时候，若文件后缀是js或者jsx，则可以省略后缀
    2. 创建组件和暴露组件分开写麻烦，可以合并（详见code/脚手架/react_staging/src/components/Hello/index.jsx）
        export default class App extends Component{}
  
4. css样式模块化（详见code/脚手架/react_staging/src/components/Hello/index.jsx）
    在css文件中间加上module，eg
    import hello from "./Hello.module.css";
    引入方式改变
    return <h2 className={hello.title}>helloworld</h2>;
5. 语法提示 vscode插件
    ES7 React/Redux/GraphQL/React-Native snippets
    1. rcc 定义类式组件
    2. rfc 定义函数式组件
*/
```
# 案例

## 第一个案例

```js
/*
	1. 动态初始化列表，如何确定将数据放在哪个组件的state中？
		某个组件使用：放在其自身的state中
		某些组件使用：放在他们共同的父组件state中（官方称此操作为：状态提升）
	2. 关于父子之间通信：
			1.【父组件】给【子组件】传递数据：通过props传递
			2.【子组件】给【父组件】传递数据：通过props传递，要求父提前给子传递一个函数
	3. 注意defaultChecked(只有第一次有用) 和 checked的区别，类似的还有：defaultValue 和 value
	4. 状态在哪里，操作状态的方法就在哪里
    5.  { ...todoObj, done } ， 将前面对象属性展开，并且用后面的done的内容替换（更新）前面已有的属性
    6. filter 选出满足条件的，参数为一个回到函数
    7. map 对所有的对象执行同一操作，参数为一个回调函数
    8. reduce 对对象列表进行迭代操作，两个参数，第一个为回调函数，第二个为结果的初始值
        其中回调函数有两个参数，第一个是前一次返回的结果，第二个为本次迭代对象本身
    9. props 限制器 
        安装    yarn  add prop-types
        导入    import PropTypes from 'prop-types'
        使用    static propTypes = {
                    addTodo: PropTypes.func.isRequired（必须是个函数）
                }
    10. 设置唯一id 
        安装    yarn add nanoid
        导入    import { nanoid } from 'nanoid'
        使用    const todoObj = { id: nanoid()}
    11. onMouseLeave 鼠标离开，onMouseEnter 鼠标进入
    12. 内联不传参，外部函数直接使用箭头函数
    13. 内联传参 
            直接传参： 
                外部函数使用箭头函数，返回值为箭头函数
            箭头函数传参 
                外部函数使用箭头函数，返回值为value

*/

// filter
const newTodos = todos.filter((todoObj) => {
    return todoObj.id !== id
})

// map
const newTodos = todos.map((todoObj) => {
    return { ...todoObj, done }
})

// reduce
const doneCount = todos.reduce((pre, todo) => {
    return pre + (todo.done ? 1 : 0)
}, 0)

// 内联传参直接法
handleCheck = (id) => {
    return (event) => {
        this.props.updateTodo(id, event.target.checked)
    }
}
onChange={this.handleCheck(id)}

//内联传参箭头函数传参
handleDelete = (id) => {
    if (window.confirm("确定删除吗？")) {
        this.props.deleteTodo(id)
    }
}
onClick={() => { this.handleDelete(id) }} 
```

### 脚手架代理配置
```js
/*
axios 安装
npm i axios
导入
import axios from 'axios'


跨域：
    场景：本地3000 端口向 5000端口的服务器发送请求，并且5000服务器将数据返回给3000，会出现跨域（可以发出去，但是返回的数据无法接受）
解决方案：
    配置代理
坑：
    若客户端已有的资源，则不会发生代理，若没有，则会在代理查询，若代理没有，报错
*/
1. 方法一：
/*
1. 优点：配置简单，前端请求资源时可以不加任何前缀。
2. 缺点：不能配置多个代理。
3. 工作方式：上述方式配置代理，当请求了3000不存在的资源时，那么该请求会转发给5000 （优先匹配前端资源）
*/
在package.json 文件配置
"proxy":"http://localhost:5000"（服务端的ip+端口），
并且将发送请求的端口地址改为发送方的端口

2. 方法二
/*
1. 优点：可以配置多个代理，可以灵活的控制请求是否走代理。
2. 缺点：配置繁琐，前端请求资源时必须加前缀。
*/
新建   setupProxy.js   文件(文件名不可修改)，会自动查找

const proxy = require('http-proxy-middleware')
module.exports = function(app) {
  app.use(
    proxy('/api1', {  //api1是需要转发的请求(所有带有/api1前缀的请求都会转发给5000)
      target: 'http://localhost:5000', //配置转发目标地址(能返回数据的服务器地址)
      changeOrigin: true, //控制服务器接收到的请求头中host字段的值
      /*
      	changeOrigin设置为true时，服务器收到的请求头中的host为：localhost:5000
      	changeOrigin设置为false时，服务器收到的请求头中的host为：localhost:3000
      	changeOrigin默认值为false，但我们一般将changeOrigin值设为true
      */
      pathRewrite: {'^/api1': ''} //去除请求前缀，保证交给后台服务器的是正常请求地址(必须配置)
    }),
    proxy('/api2', { 
      target: 'http://localhost:5001',
      changeOrigin: true,
      pathRewrite: {'^/api2': ''}
    })
  )
}
```

## github 搜索案例
```js
/*
1. 三目运算符 嵌套使用
    a?1:
    b?2:
    c?3:
    4
    上面的含义是依次判断a,b,c是否为真，做出操作
2. 使用map函数时，若显示map未定义，很有可能所传参数不是一个可遍历对象
3. 连续解构赋值
    const {keyWordElement:{value}} = this
4. /连续解构赋值并重命名
    const { keyWordElement: { value: ans } } = this
5. css中 class 改为 className
6. style 要改为style={{xx:xx}}
*/

源码查看05_src_github搜索案例
```
## 消息发布和订阅
```js
/*
pubsub-js
    安装  yarn add pubsub-js
    导入  import PubSub from 'pubsub-js'
    发布消息 PubSub.publish('mymsg',{data})
    订阅消息 PubSub.subscribe('mymsg', (msg,stateObj) => {console.log('ok')}
*/
可以实现任意组件之间的的数据传递
源码查看 06_src_github搜索案例_pubsub
```
## fetch 发送请求（关注分离）
```js
/*
fetch是一种HTTP数据请求的方式，是XMLHttpRequest（xhr）的一种替代方案。fetch不是ajax的进一步封装，而是原生js。Fetch函数就是原生js，没有使用XMLHttpRequest对象。
发送ajax 请求的有xhr（包含jquery，axios）和fetch
*/

// // 未优化
// fetch(`/api1/search/users?q=${ans}`).then(response=>{
//     console.log("联系服务器成功了");
//     return response.json();
// },
// error=>{
//     console.log("联系服务器失败了",error)
//     // 如果错误，返回空的对象，不走之后的then方法
//     return new Promise()
// }
// ).then(
//     response =>{
//         console.log("获取数据成功了",response)
//     },
//     error =>{
//         console.log("获取数据失败了",error);
//     }
// )





// 优化
fetch(`/api1/search/users?q=${ans}`).then(response=>{
    console.log("联系服务器成功了");
    return response.json();
},
).then(
    response =>{
        console.log("获取数据成功了",response)
    },
).catch(
    error=>{
        console.log('请求出错',error);
    }
)
```
# 路由
## SPA
```js
/*
    单页Web应用（single page web application，SPA），就是只有一张Web页面的应用，是加载单个HTML 页面并在用户与应用程序交互时动态更新该页面的Web应用程序。
    1. 整个应用只有一个完整的页面
    2. 点击链接不会刷新页面，只做局部更新
    3. 数据需要ajax异步请求
*/
```

## 什么是路由
```js
/*
 一个路由就是一个映射关系
 key为路径，value为function或者component
*/
```
## react-router-dom
```js
/*
react的一个插件
专门实现一个SPA应用
常用库
*/
安装
yarn add react-router-dom  
使用
    1. 导航区a标签全部替换为 Link标签
    2. 展示区写Route标签进行路径匹配
    3. <APP>在最外侧包裹一个<BrowserRouter>或者<HashRouter>(url后面有#)

import { Link, Route } from 'react-router-dom'
<Link className="list-group-item" to="/about">About</Link>
<Route path='/about' component={About} />
```
## 一般组件和路由组件
```js
/*
一般组件：<About/>,一般放在component 文件夹下
路由组建：{About} ，一般放在 pages 文件夹下
一般组件 不传props时，接收不到
路由组件 默认会传递 history，location，match属性
history:
    go: ƒ go(n)
    goBack: ƒ goBack() 
    goForward: ƒ goForward()
    push: ƒ push(path, state)
    replace: ƒ replace(path, state)
location:
    pathname: "/about"
    search: ""
    state: undefined
match:
    params: {}
    path: "/about"
    url: "/about"
*/
```
## NavLink
```js
/*
1. NavLink可以实现路由连接的高亮，通过activeClassName指定样式名
2. 标签体内容是一个特殊的标签属性
3. this.props.children 获取标签体内容
导入
import {NavLink } from 'react-router-dom'
使用和Link使用一样
*/

封装自己的myNavLink
/*
1. 在自己的样式后面加上 ！import，拥有最高权限，覆盖其他css样式
 .mycss{
        background-color: aqua !important;
        color: white !important;
    }
*/

调用
<MyNavLink to='about'>About</MyNavLink>

新建组件
export default class MyNavLink extends Component {
    render() {
        return (
            <NavLink activeClassName='mycss' className="list-group-item" {...this.props}/>
        )
    }
}
```
## 样式丢失
```js
/*
1. 导入的时候不要最前面的.
2. 加上%PUBLIC_URL%/前缀
3. 使用<HashRouter>不要使用<BrowserRouter>
yarn 安装 包
yarn add ***
npm 安装
npm i ***
*/
```

## 路由匹配机制
```js
/*
1. 默认情况下匹配到后，会接着继续匹配，将所有符合条件的展示
2. 在外侧包裹Switch标签，匹配成功后不会继续匹配
*/
模糊匹配（默认）
    1. to 后面可以多写，但是path所需要的必须要有
    2. path 所需要的，放在 to 的最前面 
    3. 若to 开始匹配不上，则认为没有匹配成功

精准匹配
    1. exact
    2. 使用后to 后面的必须和path后面的完全一致
    使用：
    <Route exact path='/about' component={About} />

Redirect
    若 路由列表里面都没有匹配成功，则会展示Redirect所指定的组件
    使用：
        <Redirect to = '/home/'></Redirect>

```
## 二级路由

```js
/*
    1. 注意：二级路由不能够使用严格模式，即不能用exact
    2. 二级路由 to 和 path 后面都需要加上父路由
*/
<MyNavLink to="/home/message">Message</MyNavLink>
<Route path="/home/message" component={Message}/>
```
## 路由传参
### params
```js
/*
1. 路由中携带参数
<Link to={`/home/message/detail/${messageObj.id}/${messageObj.title}`}>{messageObj.title}</Link>
2. 注册路由（接收）：
<Route path="/home/message/detail/:id/:title" component={Detail} />
3. 使用
const {id,title} =this.props.match.params
const findResult = data.find(detailObj=>{
    return detailObj.id === id
})
*/
```
### search
```js
/*
qs (脚手架已经下载，无需下载)对象和xx=xx&xx=xx(UrlEncode)的形式相互转换
import qs from 'querystring'
对象转urlencode
    qs.stringly(对象)
urlencode 转 对象
    qs.parse(urlencode)
*/
eg:
    let obj = { 'name': 'gc', age: 18 }
    let str = 'name=xs&age=17'
    console.log(qs.stringify(obj));
    console.log(qs.parse(str));

/*
1. 路由中携带参数
<Link to={`/home/message/detail/?id=${messageObj.id}&title=${messageObj.title}`}>{messageObj.title}</Link>
2. 注册路由无需接收参数
3. 使用
const {search} =this.props.location
const {id,title} = qs.parse(search.slice(1))

const findResult = data.find(detailObj => {
    return detailObj.id === id
})
*/
```

### state
```js
/*
1. 路由中不用携带参数
<Link to={{
    pathname: '/home/message/detail', state: {
        id: messageObj.id, title: messageObj.title
    }
}}>{messageObj.title}</Link>
2. 注册路由无需接收参数
3. 使用
const { id, title } = this.props.location.state || {}
const findResult = data.find(detailObj => {
    return detailObj.id === id
}) || {}
4. 刷新页面可以保留参数
*/
```
### replace 和push
```js
/*
1. 默认是push ，点击浏览器后退返回上一次的访问
2. replace ，会替换最新一次的记录
3. Link 后添加
4. 都可以传递三种类型参数，只要对应即可
*/
// params
replaceShow = (id,title)=>{
    this.props.history.replace(`/home/message/detail/${id}/${title}`)
}
pushShow = (id,title)=>{
    this.props.history.push(`/home/message/detail/${id}/${title}`)
}
// search
replaceShow = (id,title)=>{
    this.props.history.replace(`/home/message/detail/?id=${id}&title=${title}`)
}
pushShow = (id,title)=>{
    this.props.history.push(`/home/message/detail/?id=${id}&title=${title}`)
}
// state
replaceShow = (id,title)=>{
    this.props.history.replace(`/home/message/detail/`,{id,title})
}
pushShow = (id,title)=>{
    this.props.history.push(`/home/message/detail/`,{id,title})
}

<button  onClick={()=>this.pushShow(messageObj.id,messageObj.title)}>push查看</button>
<button onClick={()=>this.replaceShow(messageObj.id,messageObj.title)}>replace查看</button>


前进和后退
back = () => {
    //函数体
    this.props.history.goBack()
}
forword = () => {
    //函数体
    this.props.history.goForward()
}

<button onClick={this.back}>回退</button>
<button onClick={this.forword}>前进</button>
```
### withRouter
```js
/*
将普通组件修改为路由组件，使其可以使用路由组件的相关api
在你想修改的组件导入
import {withRouter} from 'react-router-dom';
使用
export default withRouter(xxx)
*/
```
## BrowserRouter 和 HashRouter 区别
```js
/*
1.底层原理不一样：
            BrowserRouter使用的是H5的history API，不兼容IE9及以下版本。
            HashRouter使用的是URL的哈希值。
2.path表现形式不一样
            BrowserRouter的路径中没有#,例如：localhost:3000/demo/test
            HashRouter的路径包含#,例如：localhost:3000/#/demo/test
3.刷新后对路由state参数的影响
            (1).BrowserRouter没有任何影响，因为state保存在history对象中。
            (2).HashRouter刷新后会导致路由state参数的丢失！！！
4.备注：HashRouter可以用于解决一些路径错误相关的问题。
*/
```
