# antd
antd 是基于 Ant Design 设计体系的 React UI 组件库，主要用于研发企业级中后台产品.
# 安装ant design
```
yarn add antd
```
#普通使用
```js
// 导入
import { Button } from 'antd';
import 'antd/dist/antd.css';
// 使用
复制即可
```
# 样式按需导入
```js
/*
yarn add react-app-rewired customize-cra
第一个是 启动项目命令，第二个是执行修改样式命令
yarn add babel-plugin-import
按需引入的库
*/
```