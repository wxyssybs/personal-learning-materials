# 什么是React

```
一个用于构建用户界面的javaScript库
一个将数据渲染为HTML视图的开源Javascript库
```

## 特点

```
1.采用组件化模式，声明式编码，提高开发效率，组件复用率
2.使用虚拟DOM，尽量减少与真实DOM交互
```

## 学习前的准备

```
javascript：
	判断this的指向
	class（类）
	ES6语法规范
	npm包管理器
	原型，原型链
	数组常用方法
	模块化
```
# 第一个代码
```js
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>react</title>
	<!-- 核心库 全局多了React -->
	<script type="text/javascript" src="./react.development.js"></script>
	<!-- 支持react操作dom 全局多了ReactDOM -->
	<script type="text/javascript" src="./react-dom.development.js"></script>
	<!-- babel转为js -->  
	<script type="text/javascript" src="./babel.min.js"></script>
	<!-- babel -->
	<script type="text/babel">
	// 不要加引号
		const VDOM = <h1 id='hid'>hello</h1>
		ReactDOM.render(VDOM, document.getElementById("idiv"))
	</script>
</head>
<body>
	<div id="idiv">
</body>
</html>
```
## 虚拟dom创建的两种方式
```js
// 注意 只要创建了虚拟dom，就必须导入前两个库
/*
方法一：
	不使用babel，第三个导入的可以删掉
	只引入前两个
*/
<script type="text/javascript">
	const VDOM = React.createElement('h1', { id: 'hid' }, 'hello')
	ReactDOM.render(VDOM, document.getElementById("idiv"))
</script>
/*
方法二：
	使用babel
	灵活，多层嵌套
*/
<script type="text/babel">
// 括号可以不加
const VDOM = (<h1>
	<span>
		<p>
			你好
		</p>
	</span>
</h1>);	
ReactDOM.render(VDOM,document.getElementById('idiv'))
</script>
```
## 语法规则
### js语句和js表达式
```js
/*
会产生返回值的都算表达式，返回为空也算
	eg：
		a
		a+b
不产生值的都算语句
	eg：
		if(){}
		for(){}

*/
```
```js
/*
jsx语法规则
	1. 定义虚拟dom，不要写引号
	2. 标签中混入js表达式(注意注意注意是表达式) 加{}
		eg:
			const myid = 'gcid'
			const mydata = 'gcdata'
			const VDOM = (<h1><span><p id= {myid}>{mydata}</p></span></h1>);
	3. 混入css，不需要{},直接用 
		eg：
			类名需要写className
			const VDOM = (<h1 className='myclass'><span>{mydata}</span></h1>);
	4. 内联样式使用,使用json格式，再嵌套{}
		eg：
			const VDOM = (<h1 style={{background:'aqua'}}>{mydata}</h1>);
			// 外层括号表示jsx中引用js表达式，内层括号表示使用json格式传递
			// style ={{key:value,key:value}}
	5. 虚拟dom只能有一个根标签，所有标签必须闭合
	6. 标签首字母
		若以小写开头，则直接将标签转为html同名元素，没有的话报错（不要写h5中没有的标签）
		若以大小开头，react就去渲染对应的组件，若没有定义该组件，则报错
*/
```
# 组件
### 组件实例的三大属性
	state,props,refs

简单组件：无状态
复杂组件：有状态
## 函数式组件
```js
/*
	1.首字母大写
	2.渲染时要使用闭合标签
*/
function Demo(){
	return <h2>demo组件的标签</h2>
	}
ReactDOM.render(<Demo/>,document.getElementById('idiv'))

```
## 类式组件
```js
/*
	1. 首字母大写
	2. 必须继承React.Component
*/
class MyComponent extends React.Component{
    render() {
        return (<div>mycomponent组件的标签</div>);
    }
}
ReactDOM.render(<MyComponent/>,document.getElementById('idiv'))
```
# state
```js

// state 设置
constructor(props){
    super(props)
    this.state={ishot:true,wind:'大'}
}

// state 获取
// 一般用法
render(){
return <h1>今天天气很{this.state.ishot ? '炎热':'凉爽'}</h1>
}
// 进阶用法 
const {wind} = this.state
/*
	1. 传入格式为dict，可以多个属性
	2. 使用进阶用法时，｛｝里面的key必须在status已经存在，可以多个同时获取
*/
```

# 事件绑定
## 一般用法
```js
/*
1. onClick c要大写
2. 调用时 ｛｝
3. function 不能加（），否则是函数返回值进行赋值（未执行操作已经出结果） 
*/
function demo(){
    console.log('点击')
    }
return <h1 onClick={demo}>今天天气很{this.state.ishot ? '炎热':'凉爽'}</h1>

```

## 事件修改state 
### 一般形式
```js
/*
注意1 ：
    为什么要有这句？？？
        因为在 onClick={this.changeweather} 中，并未调用changeweather方法，只是将其绑定给了onclick，当在页面中点击调用时，该方法已经脱离了weather类，即无获取到类中的state属性
    1. 等号右边先在下方查找changeweather（）方法，因为是在类中定义的方法，需要this调用
    2. 为1中的方法绑定this，该this是指weather类中的this，并将其赋给一个新的function，名称为this.changeweather
    3. 2中的this.changeweather 便可获取到weather中的state，onclick中调用的方法也是2中等号左边的方法
*/
class Weather extends React.Component {
constructor(props){
    super(props)
    this.state={ishot:true,wind:'大'}
    //注意1
    this.changeweather = this.changeweather.bind(this)
    }
render(){
    const {ishot,wind} = this.state
    return <h1 onClick={this.changeweather}>今天天气很{this.state.ishot ? '炎热':'凉爽'}</h1>
    }
changeweather(){
    const ishot = this.state.ishot
    // 不能够直接修改变量，即 ishot = !ishot 是错误的，必须使用setState该方法
    // 修改格式是更新，而不是全部覆盖
    this.setState({ ishot : !ishot})
    console.log(this.state.ishot)
    }
}
/*
    constructor 调用 1次
    render 调用 1+n(状态更新次数)次
    changeweather 调用 n（点击几次，调用几次）次
*/
```
### 精简模式
```js
/*
=> 箭头函数，没有自己的this，它会在当前所在地方的父级指定为this
    eg：
        下面箭头函数的父级为weather类，所以this为weather这个类

在类中直接写个赋值语句，相当于在类之后创建的所有的对象中，都会有此属性
*/
class Weather extends React.Component {
    state = { ishot: true, wind: '大' }
    render() {
        const { ishot, wind } = this.state
        return <h1 onClick={this.changeweather}>今天天气很{this.state.ishot ? '炎热' : '凉爽'}</h1>
    }
    // 箭头函数的父级
    changeweather = () => {
        const ishot = this.state.ishot
        this.setState({ ishot: !ishot })
    }
}   
```

# props
## 一般用法
```js
/*
    1. 渲染时可以直接设置属性， 多传不报错，少传不报错
    2. const {name,age,sex} = this.props 获取到不显示不报错
    3. 没有获取，显示，会报错
 
*/
class Person extends React.Component {
    render() {
        const {name,age,sex} = this.props
        return (
            <ul>
            <li>{name}</li>
            <li>{age}</li>
            <li>{sex}</li>
            </ul>
        )
    }
}
        ReactDOM.render(<Person name='gc' age='18' sex='男' />, document.getElementById('idiv1'))
        ReactDOM.render(<Person name='gc' age='18' sex='男' />, document.getElementById('idiv2'))
        ReactDOM.render(<Person name='gc' age='18' sex='男' />, document.getElementById('idiv3'))

```
## props限制
```js
// 错误示范
Person.propTypes = {
    name:React.PropTypes.string
}
/*
    直接如上写法报错，在react15以及15以前，都在维护PropTypes，react16以后，因为怕react过于庞大，所以删减了，将其放在prop-types.js文件中，想要使用，导入即可
*/
// 正确示范
<script type="text/javascript" src="./prop-types.js"></script>

// 为属性添加设置
Person.propTypes = {
    name:PropTypes.string
}

常见的属性限制
/*
    PropTypes.string  必须为字符串
    PropTypes.string.isRequired 必填
    PropTypes.number  必须为数字
    PropTypes.func  必须为函数
    
*/

// 为属性加默认值,传递的时候没有传递，自动获取
Person.defaultProps = {
     sex :'不男不女'
}

props值只可以读，不可以修改，this.props.name = 'gc' 是错误的
```
## 精简模式
```js
/*
 Person.propTypes等一系列相当于类方法，将其放在类内部，并加上static
*/
class Person extends React.Component {
    static propTypes = {
        name:PropTypes.string.isRequired,
    }
    static defaultProps = {
        sex :'不男不女'
    }
    render() {
        const {name,age,sex} = this.props
        return (
        <ul>
            <li>{name}</li>
            <li>{age}</li>
            <li>{sex}</li>
        </ul>
        )
    }
}
ReactDOM.render(<Person  name={100} age='18'  />, document.getElementById('idiv1'))

```
## 函数式组件使用props
```js
/*
    函数也可以使用props，此时类似于传参，props是形参名，可以随意
*/
function Person(a) {
    const { name, age, sex } = a
    return (
        <ul>
        <li>{name}</li>
        <li>{age}</li>
        <li>{sex}</li>
        </ul>
    )
}
```

### 小扩展
    构造器中是否接受props，是否传递给super，取决于是否希望在构造器中通过this访问props，但一般不用
# refs
## 字符串型（过时了）
```js
/*
1. 个人感觉ref就像id
2. 多用箭头函数
*/
 class Demo extends React.Component {
    showData1 =()=>{
        const {input1} = this.refs;
        alert(input1.value)
        }
    showData2 =()=>{
        const {input2} = this.refs;
        alert(input2.value)
        }
    render() {
        return (
        <div>
            <input ref="input1" type="text" placeholder="点击按钮提示数据"/>
            <button ref="btn" onClick = {this.showData1}>点我提示左边数据</button>
            <input ref="input2" onBlur = {this.showData2} type="text" placeholder="失去焦点提示数据"/>
        </div>
        )
    }
}
```
## 回调形式的ref（推荐使用）
```js
/*
    1. a代表的是当前标签
    2. (a)=>this.input2 = a 箭头函数只有一句，可以省略括号
    3. this 指的是这个类
*/
class Demo extends React.Component {
    showData1 =()=>{
        const {input1} = this;
        alert(input1.value)
    }
    showData2 =()=>{
        const {input2} = this;
        alert(input2.value)
    }
    changeweather = () => {
        const { ishot } = this.state;
        this.setState({ ishot: !ishot })
    }
    render() {
        return (
            <div>
                <input ref={ a =>{this.input1 = a;console.log(this)}} type="text" placeholder="点击按钮提示数据"/>
                <button ref="btn" onClick = {this.showData1}>点我提示左边数据</button>
                <input ref={(a)=>this.input2 = a} onBlur = {this.showData2} type="text" placeholder="失去焦点提示数据"/>
                <br/><button ref="btn1" onClick={this.changeweather}>更改天气</button>
            </div>
        )
    }
}
/*
内联方式的ref调用次数 1+2*n
    每次render都会清空上一次，所以每次都是新的function，并且赋null
*/
```
## createRef
```js
/*
    创建的myRef只能供一个使用，多次赋值会被覆盖
    所以想要多个使用，创建myref1，myref2……
*/
class Demo extends React.Component {
    myRef = React.createRef()
    howData1 = () => {
        console.log(this.myRef.current.value)
    }
    render() {
        return (
            <div>
                <input ref={this.myRef} type = "text" />
                <button ref="btn" onClick={this.showData1}>点我提示数据</button>
            </div>
        )
    }
}
```
# 事件处理
```js
/*
    1. 通过onXxx属性指定事件处理函数（注意大小写）
        1）. 使用的是合成事件，不是原生dom事件（大写了字母，react重新封装的）
        2）. 事件委托给最外层元素
    2. event.target 得到发生事件的dom元素对象
    3. 不要过度的使用ref
    4. ref 避免：
        发生事件的元素正好是你操作的元素，可以避免（事件你自己调用，并且自己出结果，不经过其他的元素）
*/
// 可以避免
<input  onBlur={this.showData2} type="text" placeholder="失去焦点提示数据" />
// 不可以避免（button不是操作自身，而是操作input）
<input ref={a => { this.input1 = a;console.log("@",a) }} type="text" placeholder="点击按钮提示数据" />
<button ref="btn" onClick={this.showData1}>点我提示左边数据</button>

```
# 非受控组件
```js
/*
1. `${}`,直接可以填入字符串，类似于py的format格式
2. event.preventDefault() 阻止页面跳转

*/
 class Login extends React.Component{
    mysubmit=(event)=>{
        event.preventDefault()
        const {username,password} = this;
        alert(`用户名是：${username.value},密码是:${password.value}`)
    }
    render(){
        return(
                <form onSubmit={this.mysubmit}>
                    用户名：<input ref ={c=>this.username = c} type='text'/>
                    密码：<input ref ={c=>this.password= c} type='password'/>
                    <button>登录</button>
                </form>
           )
        }
}
```
# 受控组件
```js
/*
    输入类的组件再输入中将value维护到state中，在使用的时候直接从state中取
    没有用 ref
*/
class Login extends React.Component {
    state ={
        username:'',
        password:''
    }
    saveusername = (event) => {
        this.setState({ username: event.target.value })
    }
    savepassword = (event) => {
        this.setState({ password: event.target.value })
    }
    mysubmit = (event) => {
        event.preventDefault()
        const { username, password } = this.state;
        alert(`用户名是：${username},密码是:${password}`)
    }
    render() {
        return (
            <form onSubmit={this.mysubmit}>
                用户名：<input onBlur={this.saveusername} type='text' />
                密码：<input onChange={this.savepassword} type='password' />
                <button>登录</button>
            </form>
        )
    }
}
```
# 高阶函数
```js
/*
    高阶函数：若一个函数符合下面2个规范的任何一个，都算
        1. 接受的参数是一个function
        2. 返回值是一个 function
    函数的柯里化： 通过函数调用继续返回函数的方式，实现多次接收参数最后统一处理的函数编码形式
    注意：
        1. onChange={this.savedata('username')} onchege接受的是一个函数，而等式右边的返回值也是一个function
        2. {[key]:event.target.value} 可以获取多个参数 类似于mydic[key]的形式
*/
class Login extends React.Component {
    state = {
        username: '',
        password: ''
    }
    savedata = (key) => {
        return (event)=>{
            this.setState({[key]:event.target.value})
        }
    }
    mysubmit = (event) => {
        event.preventDefault()
        const { username, password } = this.state;
        alert(`用户名是：${username},密码是:${password}`)
    }
    render() {
        return (
            <form onSubmit={this.mysubmit}>
                用户名：<input onChange={this.savedata('username')} type='text' />
                密码：<input onChange={this.savedata('password')} type='password' />
                <button>登录</button>
            </form>
        )
    }
}

/*
    内联式写法
*/
savedata = (key,event) => {
    this.setState({[key]:event.target.value})
}
用户名：<input onChange={this.savedata('username')} type='text' />
密码：<input onChange={this.savedata('password')} type='password' />
```
# 生命周期
```js
/*
理解：
    1. 组件从创建到死亡会经历一些特定的阶段
    2. React组件中包含一系列的钩子函数（生命周期回调函数），会在特定的时刻调用
    3. 定义组件时，会在特定的生命周期回调函数中做特定的工作

生命周期回调函数 <=> 生命周期钩子函数 <=> 生命周期函数 <=> 生命周期钩子
删除组件
    ReactDOM.unmountComponentAtNode(document.getElementById('idiv'))
render的调用时机：
    初始化渲染，状态更新
componentDidMount(){}
    组件挂载完毕调用（一次）
componentWillUnmount(){}
    组件将要卸载时调用
clearInterval()
    清除定时器
setInterval()
    创建定时器
  */
class Life extends React.Component {
    state = { opacity: 1 }
    death = () => {
        ReactDOM.unmountComponentAtNode(document.getElementById('idiv'))
    }
    componentDidMount() {
        this.timer = setInterval(() => {
            let { opacity } = this.state
            opacity -= 0.1
            if (opacity <= 0) {
                opacity = 1
            }
            this.setState({ opacity: opacity })
            // 简写{opacity}

            }, 200)
        }
    componentWillUnmount() {
         clearInterval(this.timer)
    }
    render() {
        return (
            <div>
                <h2 style={{ opacity: this.state.opacity }}>你好吗？</h2>
                <button onClick={this.death}>还可以</button>
            </div>
        )
    }
}
```
## 生命周期（旧）
```js
/*
1. 初始化阶段：由ReactDOM.render()触发---初次渲染
    1. constructor()
    2. componentWillMount()
    3. render()
    4.componentDidMount()======>常用，一般做一些初始化的事情，例如开启定时器，发送网络请求，订阅消息
2. 更新阶段：由组件内部this.setState()或父组件重新render触发
    1. shouldComponentUpdate()
    2. componentWillUpdate()
    3. rendedr()======>必须使用的一个
    4. componentDidUpdate()
3. 卸载组件：由ReactDOM.unmountComponentAtNode()触发
    1. componentWillUnmount() ======>常用，一般做一些收尾的事情，例如关闭定时器，取消订阅消息

*/

class Count extends React.Component {
    //构造器
    constructor(props) {
        console.log("Count---constructor")
        super(props);
        this.state = {
            count: 0,
        }
    }
    //自己写的回调
    add = () => {
        let { count } = this.state
        this.setState({ count: count + 1 })
    }
    death = () => {
        ReactDOM.unmountComponentAtNode(document.getElementById("idiv"))
    }
    force = () => {
        this.forceUpdate()
    }
    //组件将要挂载
    componentWillMount() {
        console.log("Count---componentWillMount")
    }
    //组件挂载完成
    componentDidMount() {
        console.log("Count---componentDidMount")
    }
    //组件将要卸载
    componentWillUnmount() {
        console.log("Count---componentWillUnmount")
    }
    //控制组件是否更新
    shouldComponentUpdate() {
        console.log("Count---shouldComponentUpdate")
        return true
    }
    //组件将要更新
    componentWillUpdate() {
        console.log("Count---componentWillUpdate")
    }
    //组件更新完毕
    componentDidUpdate() {
        console.log("Count---componentDidUpdate")
    }
    render() {
        console.log("Count---render")
        const { count } = this.state
        return (
            <div>
                <h2>当前为{count}</h2>
                <button onClick={this.add}>点击+1</button>
                <button onClick={this.death}>卸载组件</button>
                <button onClick={this.force}>不更改，强制刷新组件</button>
            </div>
        )
    }
}

//父组件A
class A extends React.Component {
    state = {carName:'奔驰'}
    changeCar = ()=>{
        this.setState({carName:'奥拓'})
    }
    render() {
        return (
            <div>
                <div>我是A</div>
                <button onClick={this.changeCar}>换车</button>
                <B carName = {this.state.carName}/>
            </div>
        )
    }
}
//子组件B 
class B extends React.Component {
    //组件将要接受新的props的钩子（第一次不算，更新才算）
    componentWillReceiveProps(){
        console.log('B------componentWillReceiveProps');
    }
    //组件更新阀门
    shouldComponentUpdate(){
        console.log('B---shouldComponentUpdate')
        return true
    }
        //组件将要更新
        componentWillUpdate() {
        console.log("B---componentWillUpdate")
    }
    //组件更新完毕
    componentDidUpdate() {
        console.log("B---componentDidUpdate")
    }
    render() {
        console.log('B---render')
        return (
            <div>B,接受的车是{this.props.carName}</div>
        )
    }
}
```
## 生命周期（新）
```js
/*
新版本中 componentWillMount，componentDidUpdate，componentWillReceiveProps 这三个需要加上前缀'UNSAFE_'（不是指安全性，是说未来版本中可能会有bug）,
即UNSAFE_componentWillMount，UNSAFE_componentDidUpdate，UNSAFE_componentWillReceiveProps 

1. 初始化阶段：由ReactDOM.render()触发---初次渲染
    1. constructor()
    2. getDerivedStateFromProps()
    3. render()
    4.componentDidMount()
2. 更新阶段：由组件内部this.setState()或父组件重新render触发
    1. getSnapshotBeforeUpdate()
    2. shouldComponentUpdate()
    3. rendedr()
    4. componentDidUpdate()
3. 卸载组件：由ReactDOM.unmountComponentAtNode()触发
    1. componentWillUnmount()
*/
class Count extends React.Component {
    //构造器
    constructor(props) {
        console.log("Count---constructor")
        super(props);
        let {count} = props 
        this.state = {
            count: count,
        }
    }
    //自己写的回调
    add = () => {
        let { count } = this.state
        this.setState({ count: count + 1 })
    }
    death = () => {
        ReactDOM.unmountComponentAtNode(document.getElementById("idiv"))
    }
    force = () => {
        this.forceUpdate()
    }
    //若state的值任何时候都取决于props，使用该方法
    static getDerivedStateFromProps(props, state) {
        console.log("####", props, state)
        console.log("Count---getDerivedStateFromProps")
        // return props
        return null
    }
    //更新之前获取快照
    getSnapshotBeforeUpdate() {
        console.log('Count---getSnapshotBrforeUpdate')
        // return null
        return 'wxyssybs'

    }

    //组件挂载完成
    componentDidMount() {
        console.log("Count---componentDidMount")
    }
    //组件将要卸载
    componentWillUnmount() {
        console.log("Count---componentWillUnmount")
    }
    //控制组件是否更新
    shouldComponentUpdate() {
        console.log("Count---shouldComponentUpdate")
        return true
    }
    //组件更新完毕(参数，更新前的值,不是当前,snapshotValue为保存的快照值，getSnapshotBeforeUpdate的返回值)
    componentDidUpdate(preProps,preState,snapshotValue) {
        console.log("Count---componentDidUpdate",preProps,preState,snapshotValue)
    }
    render() {
        console.log("Count---render")
        const { count } = this.state
        return (
            <div>
                <h2>当前为{count}</h2>
                <button onClick={this.add}>点击+1</button>
                <button onClick={this.death}>卸载组件</button>
                <button onClick={this.force}>不更改，强制刷新组件</button>
            </div>
        )
    }
}
```
# Diffing 算法
```js
/*
    1. render的时候，发现未改变的标签，则原样输出，即输出原标签
    2. 发现改变的标签，则会输出新的标签
    3. 检测的最小单位为一个标签
*/
```
# key
```js
/*
1. 作用：
    简单讲：
        key是虚拟DOM中对象的标识，在更新显示时key起着极其重要的作用
    详细讲：
        当状态中的数据发生变化时，react会根据【新数据】生成【新的虚拟DOM】，随后React进行【新虚拟DOM】与【旧虚拟DOM】的diff比较，规则如下：
            a:旧虚拟DOM中找到了与新虚拟DOM相同的key
                1. 旧虚拟DOM中内容没变，直接使用之前的真实DOM
                2. 虚拟DOM内容改变，生成新的真实DOM，替换掉之前的真实DOM
            b:旧虚拟DOM中未找到与新虚拟DOM相同的key
                根据数据创建新的真实DOM，渲染到页面
2. 使用index作为key可能会引发的问题：
    1.对数据进行逆序添加，逆序删除等破坏顺序的操作，产生没必要的DOM更新，没问题，效率低
    2. 若结构中包含输入类的DOM，残生错误额的DOM更新，（diffing算法）
    3. 若不破坏顺序的操作，仅用于展示，则可以使用index作为key
3. 如何选择？
    1. 最好使用唯一标识作为key
    2. 若只是简单地展示。可以用index
*/
```
react 中使用ajax，不推荐用jquery，太大
使用axios，博主不会，先去学习一番
待更...