

# linux和git快捷键



```
搜索历史命令
ctrl+r（最近的），若不是，继续ctrl+r ，
当显示到你想找的合适的历史命令的时候，直接 [Enter]，就执行了历史命令。

命令行内操作
Ctrl + a-- 跳到行首
Ctrl + e-- 跳到行尾

2. 删除操作快捷键
Ctrl + d-- 向右删除一个字符
Ctrl + h-- 向左删除一个字符
Ctrl + u-- 删除当前位置字符至行首（输入密码错误的时候多用下这个）
Ctrl + k-- 删除当前位置字符至行尾
Ctrl + w-- 删除从光标到当前单词开头

3.其他操作快捷键
Ctrl + y-- 插入最近删除的单词
Ctrl + c-- 终止操作
Ctrl + d-- 当前操作转到后台
Ctrl + l-- 清屏 （有时候为了好看）
```

# mysql 快捷键

```
查找
pager grep xxx;
select * from aaa;
取消 nopager

查看表中有哪个字段
列名
select count(*) from information_schema.columns where table_name = '表名' and column_name = '字段名'
所有的
pager grep key; select * from 表名;
```

