# JQuery
jQuery是一个JavaScript函数库。

jQuery是一个轻量级的"写的少，做的多"的JavaScript库。

# 基本语法
先引入jquery
```js
$(document).ready(function(){
    $("#btn").click(function(){
        var name = $("#name").val()
        console.log(name)
    })
})
/* 	. 代表 class
	# 代表 id
    函数： $(xxx)
    对象： $.xxx()
*/

```
# 获取修改元素
```js
/*
	value 获取
		$("input").val()
	标签内的值获取
		$("input").html()
	修改
		直接传参即可
*/
$(document).ready(function(){
    $("#btn").click(function(){
		$(this).html("button")
	})
})
/*
	index
		当前元素在兄弟元素中的下标
*/
$("#i").index()
```
# 添加元素
```js
/*
添加到后面
	appendTo()
添加到前面
	prependTo()
*/
 $(document).ready(function(){
    $("#btn").click(function(){
		$("<p>新加的pppp</p>").appendTo('#idiv')
	})
})
```
# 选择器
```js
/*
	取并集 ,
		$("#idiv,#idiv1").css({"background":"red"})
	取交集 (为div并且id=div1)
		$("div#idiv1").css({"background":"red"})
	取指定元素的子元素 (div下的所有p标签)
		$("div p").css("background","red")
	取指定元素的子元素 (div下的直系子元素p标签)
		$("div>p").css("background","red")	
	取指定元素的紧邻下一个元素 (id为idiv的下一个div元素)
		$("#idiv+div").css("background","red")
	取指定元素的后面所有的指定兄弟元素 (id为idiv的后面所有的div元素)
		$("#idiv~div").css("background","red")
	取页面中的第一个div
		$("div:first").css("background","red")
	取页面中的最后一个div
		$("div:last").css("background","red")
	取class不为idiv的div标签
		$("div:not(.box)").css("background","red")		

*/
$("#idiv").css({"background":"red"})

```
# 属性
```js
/*
attr()
	操作属性值为非bool的
prop()
	操作属性值为bool的
*/
获取属性
	console.log($("div").attr('style'))
设置（覆盖）属性
	$("div").attr('style','background-color: antiquewhite;')
删除属性
	$("div").removeAttr('style')
添加class
	$("div").addClass('xxx')
删除class
	$("div").removeClass('xxx')
```

# 多库共存
```js
/*
	如果两个库都有$,
	那么jq可以使用jquery代替$
*/
jQuery("div").css('background','red')

```
# onload 和 ready
```js
/*
$(document).ready(function() {} 
简写为
$(function(){})
*/
/*
window.onload 
页面和图片加载完才会触发
ready
页面加载完就会触发
*/
```

