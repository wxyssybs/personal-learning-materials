# 什么是Supervisor？

​	Supervisor安装与配置(linux/unix进程管理工具) Supervisor（[Supervisor: A Process Control System](https://link.zhihu.com/?target=http%3A//supervisord.org)）是用Python开发的一个client/server服务，是Linux/Unix系统下的一个进程管理工具，不支持Windows系统。它可以很方便的监听、启动、停止、重启一个或多个进程。用Supervisor管理的进程，当一个进程意外被杀死，supervisort监听到进程死后，会自动将它重新拉起，很方便的做到进程自动恢复的功能，不再需要自己写shell脚本来控制。

因为Supervisor是Python开发的,安装前先检查一下系统否安装了Python2.4以上版本。

# 使用

## 安装

```
pip install supervisor
```

## 配置文件详解

注意  ：  ；表示注释

### 配置文件：/etc/supervisord.conf

```
#个人常用配置文件参数详解
[unix_http_server]
file=/tmp/supervisor.sock   ;UNIX socket 文件，supervisorctl 会使用
;chmod=0700                 ;socket文件的mode，默认是0700
;chown=nobody:nogroup       ;socket文件的owner，格式：uid:gid
 
;[inet_http_server]         ;HTTP服务器，提供web管理界面
;port=127.0.0.1:9001        ;Web管理后台运行的IP和端口，如果开放到公网，需要注意安全性
;username=user              ;登录管理后台的用户名
;password=123  
……
;包含其它配置文件
[include]
;和子目录的路径对应
files = supervisord.d/*.ini
```

### 默认配置子文件目录：/etc/supervisord.d

```
#项目名(展示用)
[program:gc_shell]
#脚本目录(会先进入这个目录)
directory=/opt/code/python
#脚本执行命令
command=python3 /opt/code/python/1.py

#supervisor启动的时候是否随着同时启动，默认True
autostart=true
#当程序exit的时候，这个program不会自动重启,默认unexpected，设置子进程挂掉后自动重启的情况，有三个选项，false,unexpected和true。如果为false的时候，无论什么情况下，都不会被重新启动，如果为unexpected，只有当进程的退出码不在下面的exitcodes里面定义的
autorestart=false
#这个选项是子进程启动多少秒之后，此时状态如果是running，则我们认为启动成功了。默认值为1
startsecs=1

#脚本运行的用户身份 
user = test

#日志输出 
stderr_logfile=/tmp/blog_stderr.log 
stdout_logfile=/tmp/blog_stdout.log 
#把stderr重定向到stdout，默认 false
redirect_stderr = true
#stdout日志文件大小，默认 50MB
stdout_logfile_maxbytes = 20M
#stdout日志文件备份数
stdout_logfile_backups = 20
```

## 常用命令

```
supervisord  启动
supervisorctl reload  重启
supervisorctl status 查看状态
supervisorctl start xxxx 开始某个进程
supervisorctl stop xxxx  停止某个进程
supervisorctl restart xxxx 重启某个进程（不会加载新的配置文件，若要更新配置文件，使用supervisorctl reload）
```

